<?php
function products() {
  $get = mysqli_query($GLOBALS['connection'], 'SELECT ProductID, ProductName, ProductCategory, ProductPrice FROM products WHERE quantity >0 ORDER BY ProductID DESC');
  if (mysqli_num_rows($get) == 0) {
    echo 'Your cart is empty';
  } else 
  {
    ?>
<table>
<?php
    while ($get_row = mysqli_fetch_assoc($get)) {
      echo '<tr><td>' . $get_row['ProductName'] . '</td><td>' . $get_row['ProductCategory'] . '</td><td>' . number_format($get_row['ProductPrice'], 2) . '</td><td><a href="../login/loginpagecheck.php?add=' . $get_row['ProductID'] . '">Add</a></td></tr>';
    }

  }

}
?>