<?php include '../init.php'; // include connection to database and start session
include '../checkout/checkout.php'; // include checkout to enable function use
?>
<html lang="en" id="index">
	<head>
		<title>Login/Register</title>
		
		<link rel="stylesheet" href="../css/custom1.css">
		<link href='http://fonts.googleapis.com/css?family=Hammersmith+One|Tangerine' rel='stylesheet' type='text/css'>
	</head>
	<body>
<img id="logo" src="../images/logo.fw.png" alt="logo" >
				<header>
					<?php
if(isset($_SESSION['username']))//check user is logged in 
{
					?>
						<nav id="horizontal_nav1">
<li>
	<a href="../login/loginpage.php">Hello <?php echo $_SESSION['name']. "<a href=\"../login/logout.php\"> logout</a>"; ?></a>
						</li>
						<li>
	<a href="../login/loginpagecheck.php">Basket</a>
						</li>
						</nav>
						<?php
}
else {
						?>
							<nav id="horizontal_nav1">
<li>
							<a href="../login/loginpage.php">Login/Register</a>
						</li>
						</nav>

						<?php
					}
					?>
					<nav id="horizontal_nav">
						<li>
							<a href="../index.php">Home</a>
						</li>
						<li id="selected">
							<a href="../PC.php">PC</a>
						</li>
						<li>
							<a href="../LapTab.php">Laptops/Tablets</a>
						</li>
						<li>
							<a href="../TV.php">TV & Entertainment</a>
						</li>
							 <li>
							<a href="../Mobiles.php">Mobiles</a>
						</li>
					</nav>

				</header>

<br/>
<br/>
<br/>
<br/>
	
<?php
if (!isset($_SESSION["username"]))  // if no username is set, set the login error and echo it
{ 
      $_SESSION["Login_Error"] = "You must be logged in to view this page.<br/>If you are a returing cutomer, please login. <br/> If you are a new customer please register below<br/>"; 
} 

if(isset($_SESSION['Login_Error']))
{
	
	echo $_SESSION['Login_Error'].'<br/><br/>';
	include '../login/sidebar.php'; // if the login error is set include sidbar and login form and registration form 
	include '../login/registerform.php';
	
}
else
{
	?>
	<section id="checkoutpage">
		<?php
 
      echo "<h2>Shopping cart for ". $_SESSION["name"] . "</h2>";  //shopping cart for the user
      echo "Payment details for ". $_SESSION["username"] . "<br/><br/><br/>";

	cart();
?>

 <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_cart">
    <input type="hidden" name="upload" value="1">
    <input type="hidden" name="business" value="jackckenna@hotmail.co.uk">
    <?php paypal_items1(); ?>
    <input type="hidden" name="currency_code" value="GBP">
    <input type="hidden" name="amount" value="<?php echo $value; ?>">
    <input type="image" src="http://www.paypal.com/en_US/i/btn/x-click-but03.gif" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
  </form>
<?php
}
?>
</section>
			
	</body>
</html>