<html lang="en" id="index">
<head>
		<link rel="stylesheet" href="../custom.css">
		 <script type="text/javascript" src="../js/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    		$('#feedback').load('../login/check.php'); //load check.php for validation 

    		$('#email').keyup(function() { //keyup function updates without reloading the page 
    			$.post('../login/check.php', { email: register.email.value }, //check the register form, email feild and value of the feild 
    				function(result) {
$('#feedback').html(result).show(); //show results 
    			});
    		});
    	
    		$('#feedbackpass').load('../login/passcheck.php'); //load passcheck.php for validation 

    		$('#password').keyup(function() {
    			$.post('../login/passcheck.php', { password: register.password.value },
    				function(result) {
$('#feedbackpass').html(result).show(); //show results 
    			});
    		});
});
    	</script>
</head>
<form method="post" action="../login/registercustomer.php" id="register">
<fieldset>
		<legend>
			Sign up here 
		</legend>
		<label for="firstname">First Name: </label><br>
		<input type="text" name="firstname" /><br><br>
		<label for="lastname">Last Name: </label><br>
		<input type="text" name="lastname" /><br><br>
		<label for="email">Email: </label><br>
		<input type="text" name="email" id="email"/><br>
		<div id="feedback"></div><br>

		<label for="password">Password: </label><br>
		<input type="password" name="password" id="password" /><br>
		<div id="feedbackpass"></div><br>

		<input type="submit" value="Submit" name="Submit"/>
		<input type="reset" value="Clear" /><br><br>
		<?php
		
if (isset($_SESSION['error1']))
{
	echo $_SESSION['error1']; //if there is an error display that error here 
	unset($_SESSION['error1']); //unset the error so it is no longer visable 
}
?>
</fieldset>
	</form>
</html>